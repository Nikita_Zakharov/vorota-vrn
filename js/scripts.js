var hamburger = document.getElementById('menu-btn');
var buttons = document.getElementsByClassName('main__service-item_btn');
var closeBtns = document.getElementsByClassName("close-btn");
var overlay = document.getElementById('overlay');
var menuLink = document.getElementsByClassName("mobile-link");
var mobileMenu = document.getElementById("mobile-menu");
var modals = document.getElementsByClassName("modal-window");
var recallBtn = document.getElementsByClassName("recall-btn");
overlay.onclick = function () {
  document.getElementsByClassName('mobile-menu__ul')[0].classList.toggle("open-menu");
  hamburger.classList.toggle("hamburger-open");
  overlay.classList.toggle("open-overlay");
};
hamburger.onclick = function () {
  document.getElementsByClassName('mobile-menu__ul')[0].classList.toggle("open-menu");
  hamburger.classList.toggle("hamburger-open");
  overlay.classList.toggle("open-overlay");
  for (i = 0; i < menuLink.length; i++) {
    menuLink[i].onclick = function () {
      document.getElementsByClassName('mobile-menu__ul')[0].classList.toggle("open-menu");
      hamburger.classList.toggle("hamburger-open");
      overlay.classList.toggle("open-overlay");
    }
  };
};
for (let a = 0; a < buttons.length; a++) {
  buttons[a].onclick = function () {
    modals[a].classList.toggle("active");
    document.getElementById('modal-overlay').classList.toggle("active");
  }
};
var _loop = function _loop(a) {
  buttons[a].onclick = function () {
    modals[a].classList.toggle("active");
    document.getElementById('modal-overlay').classList.toggle("active");
  };
};
for (var a = 0; a < buttons.length; a++) {
  _loop(a);
};
var _loop = function _loop(w) {
  recallBtn[w].onclick = function () {
    document.getElementsByClassName('modal-window')[w].classList.toggle("active");
    document.getElementById('modal-overlay').classList.toggle("active");
  };
};
for (var w = 0; w < recallBtn.length; w++) {
  _loop(w);
};
for (let c = 0; c < closeBtns.length; c++) {
  closeBtns[c].onclick = function () {
    modals[c].classList.toggle("active");
    document.getElementById('modal-overlay').classList.toggle("active");
  }
};
var _loop = function _loop(c) {
  closeBtns[c].onclick = function () {
    modals[c].classList.toggle("active");
    document.getElementById('modal-overlay').classList.toggle("active");
  };
};
for (var c = 0; c < closeBtns.length; c++) {
  _loop(c);
};
$(document).ready(function () {
  $('.go_to').click(function () {
    var scroll_el = $(this).attr('href');
    if ($(scroll_el).length != 0) {
      $('html, body').animate({
        scrollTop: $(scroll_el).offset().top
      }, 1000);
    }
    return false;
  });
});

// Отправка заявки 
$(document).ready(function() {
	$('#form').submit(function() { // проверка на пустоту заполненных полей. Атрибут html5 — required не подходит (не поддерживается Safari)
		if (document.form.name.value == '' || document.form.phone.value == '' ) {
			valid = false;
			return valid;
		}
		$.ajax({
			type: "POST",
			url: "sendform-small.php",
			data: $(this).serialize()
		}).done(function() {
			$('.order-modal').fadeIn();
			$(this).find('input').val('');
			$('#form').trigger('reset');
		});
		return false;
	});
});

// Закрыть попап «спасибо»
$('.order-modal__close').click(function() { // по клику на крестик
	$('.order-modal').fadeOut();
});

// Up btn
$(window).scroll(function () {
  var scroll = $(window).scrollTop();
  if (scroll >= 800) {
    $('.up-btn').fadeIn();
  } else {
    $('.up-btn').fadeOut();
  }
});
